$('#makeRes').click(function () {
    // var formData=$('#addReservationForm').serialize();
    var custName=$('#c1').val();
    var NIC=$('#c2').val();
    var tel=$('#c3').val();
    var dateIn=$('#c4').val();
    var dateOut=$('#c5').val();
    var roomCategory=$('#roomNameList').find(":selected").text();
    var noOfRoom=$('#noOfRooms').val();
    var totalPri=$('#totalPrice').val();
    alert(roomCategory+" "+noOfRoom+" "+totalPri)

    // var roomName = $('#roomNameList').find(":selected").text();
    $.ajax({
        url: "api/services/ClientServices.php",
        method: "GET",
        async: true,
        data: {'custName':custName, 'nic':NIC, 'tel':+ tel, 'checkIn':dateIn, 'checkOut':dateOut, 'room':roomCategory, 'no':noOfRoom, 'pr':totalPri},
        dataType: "text"
    }).done(function (resp) {
        alert("Reservation Done")
    });
});


$('#calculateTotal').click(function () {
    var formData=$('#addReservationForm').serialize();
    $.ajax({
        url: "api/services/adminServices.php",
        method: "GET",
        async: true,
        data: formData+"&operation=CalculateTotal",
        dataType: "json"
    }).done(function (resp) {
        var count = $('#noOfRooms').val();
        console.log(count);
        var price=$('#roomPrice').val();
        // var count=$('#noOfRooms').val();
        var total=price*count*resp;
        $('#totalPrice').val(total);

    });

});

$('#roomNameList').change(function() {
    $("#noOfRooms").empty();
    var formData=$('#roomDetailsForm').serialize();
    // var conceptName = $('#roomDetailsForm').find(":selected").text();
    // $('#hidden').val(conceptName);
    // // var formData=$('#roomNameForPrice').serialize();
    $.ajax({
        url: "api/services/adminServices.php",
        method: "GET",
        async: true,
        data: formData+"&operation=getAllRoomPrice",
        dataType: "json"
    }).done(function (resp) {
        $("#roomPrice").val(resp[0]);
        $("#nlable").text(resp[1]+" Rooms are Available");

    });


});

$(document).ready(function () {
    $("#roomNameList").empty();

    $.ajax({
        url: "api/services/adminServices.php",
        method: "GET",
        async: true,
        data: "&operation=getAllRoomNames",
        dataType: "json"
    }).done(function (resp) {
        for (var i in resp) {
            var name = resp[i];
            // console.log(customer[0]);
            var roomType = name[0];

            var row = $('<option >'+roomType+'</option>');
            $("#roomNameList").append(row);

        }
    });
});

$(document).ready(function () {

    $.ajax({
        url: "api/services/adminServices.php",
        method: "GET",
        async: true,
        data: "&operation=getAll",
        dataType: "json"
    }).done(function (resp) {
            var Bachelor = resp[0];
            var broomType = Bachelor[1];
            var bprice=Bachelor[3];
            var badult=Bachelor[4];
            var bbedSize=Bachelor[5];
            var bbedCoutn=Bachelor[6];

            $('#bp1').text(bprice);
            $('#bp2').text("Adults : "+badult);
            $('#bp3').text("Room Type: "+broomType);

            $('#bp5').text("Bed Size : "+bbedSize);
            $('#bp6').text("Bed Count : "+bbedCoutn);

            var couple = resp[1];
            var croomType = couple[1];
            var cprice=couple[3];
            var cadult=couple[4];
            var cbedSize=couple[5];
            var cbedCoutn=couple[6];

            $('#bp7').text(cprice);
            $('#bp8').text("Adults : "+cadult);
            $('#bp9').text("Room Type: "+croomType);

            $('#bp11').text("Bed Size : "+cbedSize);
            $('#bp12').text("Bed Count : "+cbedCoutn);

            var classic = resp[2];
            var clroomType = classic[1];
            var clprice=classic[3];
            var cladult=classic[4];
            var clbedSize=classic[5];
            var clbedCoutn=classic[6];

            $('#bp13').text(clprice);
            $('#bp14').text("Adults : "+cladult);
            $('#bp15').text("Room Type: "+clroomType);

            $('#bp17').text("Bed Size : "+clbedSize);
            $('#bp18').text("Bed Count : "+clbedCoutn);

            var double = resp[3];
            var droomType = double[1];
            var dprice=double[3];
            var dadult=double[4];
            var dbedSize=double[5];
            var dbedCoutn=double[6];

            $('#bp19').text(dprice);
            $('#bp20').text("Adults : "+dadult);
            $('#bp21').text("Room Type: "+droomType);

            $('#bp23').text("Bed Size : "+dbedSize);
            $('#bp24').text("Bed Count : "+dbedCoutn);

            var family = resp[4];
            var froomType = family[1];
            var fprice=family[3];
            var fadult=family[4];
            var fbedSize=family[5];
            var fbedCoutn=family[6];

            $('#bp25').text(fprice);
            $('#bp26').text("Adults : "+fadult);
            $('#bp27').text("Room Type: "+froomType);

            $('#bp29').text("Bed Size : "+fbedSize);
            $('#bp30').text("Bed Count : "+fbedCoutn);

            var presi = resp[5];
            var proomType = presi[1];
            var pprice=presi[3];
            var padult=presi[4];
            var pbedSize=presi[5];
            var pbedCoutn=presi[6];

            $('#bp31').text(pprice);
            $('#bp32').text("Adults : "+padult);
            $('#bp33').text("Room Type: "+proomType);

            $('#bp35').text("Bed Size : "+pbedSize);
            $('#bp36').text("Bed Count : "+pbedCoutn);

            var vip = resp[6];
            var rroomType = vip[1];
            var rprice=vip[3];
            var radult=vip[4];
            var rbedSize=vip[5];
            var rbedCoutn=vip[6];

            $('#bp37').text(rprice);
            $('#bp38').text("Adults : "+radult);
            $('#bp39').text("Room Type: "+rroomType);

            $('#bp41').text("Bed Size : "+rbedSize);
            $('#bp42').text("Bed Count : "+rbedCoutn);

    });
});


