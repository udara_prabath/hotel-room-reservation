$('#LogOut').click(function () {
    // alert("click");
    $.ajax({
        url: "api/services/adminServices.php",
        method: "GET",
        async: true,
        data:"operation=quit"
    }).done(function (resp) {
        // alert(resp);
    });
});
$(document).ready(function () {
    $("#roomTableBody").empty();
    $.ajax({
        url: "api/services/adminServices.php",
        method: "GET",
        async: true,
        data: "&operation=getAll",
        dataType: "json"
    }).done(function (resp) {
        for (var i in resp) {
            var room = resp[i];
            // console.log(customer[0]);
            var roomType = room[1];
            var category = room[2];
            var totalRooms = room[7];
            var available = room[8];
            var booked = room[9];
            var row = "<tr><td>" + roomType + "</td><td>" + category + "</td><td>" + totalRooms + "</td><td>" + available + "</td><td>" + booked + "</td></tr>";
            $("#roomTableBody").append(row);

        }
    });
});

$('#viewAllRooms').click(function () {
    $("#roomTableBody").empty();
    var formData = $('#searchRoom').serialize();
    $.ajax({
        url: "api/services/adminServices.php",
        method: "GET",
        async: true,
        data: formData + "&operation=getAll",
        dataType: "json"
    }).done(function (resp) {
        for (var i in resp) {
            var room = resp[i];
            // console.log(customer[0]);
            var roomType = room[1];
            var category = room[2];
            var totalRooms = room[7];
            var available = room[8];
            var booked = room[9];
            var row = "<tr><td>" + roomType + "</td><td>" + category + "</td><td>" + totalRooms + "</td><td>" + available + "</td><td>" + booked + "</td></tr>";
            $("#roomTableBody").append(row);

        }
    });
});

$('#searchRoombtn').click(function () {
    var formData = $('#searchRoom1').serialize();

    $.ajax({
        url: "api/services/adminServices.php",
        method: "GET",
        async: true,
        data: formData + "&operation=search",
        dataType: "json"
    }).done(function (resp) {

            var category = resp[2];
            var price = resp[3];
            var adultcount = resp[4];
            var bedsize = resp[5];
            var bedCount = resp[6];
            var totalroom=resp[7];

            $("#category").val(category);
            $("#price").val(price);
            $("#adultsCount").val(adultcount);
            $("#bedSize").val(bedsize);
            $("#bedCount").val(bedCount);
            $("#totalRooms").val(totalroom);


    });
});

$('#updateRoom').click(function () {
    var formData=$('#RoomSearchResult').serialize();
    $.ajax({
        url: "api/services/adminServices.php",
        method: "GET",
        async: true,
        data: formData +"&operation=update",
        dataType: "json"
    }).done(function (resp) {
        alert("Room Updated")
    });
});

$(document).ready(function () {
    $("#roomNameList").empty();

    $.ajax({
        url: "api/services/adminServices.php",
        method: "GET",
        async: true,
        data: "&operation=getAllRoomNames",
        dataType: "json"
    }).done(function (resp) {
        for (var i in resp) {
            var name = resp[i];
            // console.log(customer[0]);
            var roomType = name[0];

            var row = $('<option >'+roomType+'</option>');
            $("#roomNameList").append(row);

        }
    });
});


$('#roomNameList').change(function() {
    $("#noOfRooms").empty();
        var formData=$('#roomDetailsForm').serialize();
        // var conceptName = $('#roomNameList').find(":selected").text();
        // $('#hidden').val(conceptName);
        // // var formData=$('#roomNameForPrice').serialize();
        $.ajax({
            url: "api/services/adminServices.php",
            method: "GET",
            async: true,
            data: formData+"&operation=getAllRoomPrice",
            dataType: "json"
        }).done(function (resp) {
            $("#roomPrice").val(resp[0]);
            $("#nlable").text(resp[1]+" Rooms are Available");

        });


});


$('#calculateTotal').click(function () {
    var formData=$('#addReservationForm').serialize();
    $.ajax({
        url: "api/services/adminServices.php",
        method: "GET",
        async: true,
        data: formData+"&operation=CalculateTotal",
        dataType: "json"
    }).done(function (resp) {
        var count = $('#noOfRooms').val();
        console.log(count);
        var price=$('#roomPrice').val();
        // var count=$('#noOfRooms').val();
        var total=price*count*resp;
        $('#totalPrice').val(total);

        //
        // var formData2=$('#roomDetailsForm').serialize();
        // $.ajax({
        //     url: "api/services/adminServices.php",
        //     method: "GET",
        //     async: true,
        //     data: formData2+"&operation=saveTotal",
        //     dataType: "text"
        // }).done(function (q) {
        //     console.log(q);
        // });
    });

});

$('#makeRes').click(function () {
    // var formData=$('#addReservationForm').serialize();
    var custName=$('#c1').val();
    var NIC=$('#c2').val();
    var tel=$('#c3').val();
    var dateIn=$('#c4').val();
    var dateOut=$('#c5').val();
    var roomCategory=$('#roomNameList').find(":selected").text();
    var noOfRoom=$('#noOfRooms').val();
    var totalPri=$('#totalPrice').val();
    alert(roomCategory+" "+noOfRoom+" "+totalPri)

    // var roomName = $('#roomNameList').find(":selected").text();
    $.ajax({
        url: "api/services/ClientServices.php",
        method: "GET",
        async: true,
        data: {'custName':custName, 'nic':NIC, 'tel':+ tel, 'checkIn':dateIn, 'checkOut':dateOut, 'room':roomCategory, 'no':noOfRoom, 'pr':totalPri},
        dataType: "text"
    }).done(function (resp) {
        alert("Reservation Done")
    });
});

$('#getAllReservation').click(function () {
    $("#ReservationTable").empty();
    $.ajax({
        url: "api/services/adminServices.php",
        method: "GET",
        async: true,
        data: "&operation=getAllReservation",
        dataType: "json"
    }).done(function (resp) {
        for (var i in resp) {
            var room = resp[i];
            // console.log(customer[0]);
            var category = room[0];
            var customer = room[1];
            var nic = room[2];
            var noofRoom = room[3];
            var checkIn = room[4];
            var checkOut = room[5];
            var price = room[6];
            var row = "<tr><td>" + category + "</td><td>" + customer + "</td><td>" + nic + "</td><td>" + noofRoom + "</td><td>" + checkIn + "</td><td>" + checkOut + "</td><td>" + price + "</td></tr>";

            $("#ReservationTable").append(row);

        }
    });
});

$(document).ready(function () {
    $("#ReservationTable").empty();
    $.ajax({
        url: "api/services/adminServices.php",
        method: "GET",
        async: true,
        data: "&operation=getAllReservation",
        dataType: "json"
    }).done(function (resp) {
        for (var i in resp) {
            var room = resp[i];
            // console.log(customer[0]);
            var category = room[0];
            var customer = room[1];
            var nic = room[2];
            var noofRoom = room[3];
            var checkIn = room[4];
            var checkOut = room[5];
            var price = room[6];
            var row = "<tr><td>" + category + "</td><td>" + customer + "</td><td>" + nic + "</td><td>" + noofRoom + "</td><td>" + checkIn + "</td><td>" + checkOut + "</td><td>" + price + "</td></tr>";

            $("#ReservationTable").append(row);

        }
    });
});

$('#searchReservationbtn').click(function () {

    $.ajax({
        url: "api/services/adminServices.php",
        method: "GET",
        async: true,
        data: "&operation=getAllReservation",
        dataType: "json"
    }).done(function (resp) {
            var room = resp[0];
            // console.log(customer[0]);
            var categor = room[0];
            var custome = room[1];
            var ni = room[2];
            var noofRoo = room[3];
            var checkI = room[4];
            var checkOu = room[5];
            var pric = room[6];
            // alert(categor);
            $("#category").val(categor);
            $("#customer").val(custome);
            $("#nic").val(ni);
            $("#roomCount").val(noofRoo);
            $("#checkIn0").val(checkI);
            $("#checkOut0").val(checkOu);
            $("#price2").val(pric);


    });
});

