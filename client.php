<?php
    session_start();
//    session_unset();
//    session_destroy();

    if(!isset($_SESSION['login'])){
        header("Location:index.php");
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>UdR Reservation</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="dist/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="dist/css/animate.css">
    <link rel="stylesheet" href="dist/css/owl.carousel.min.css">
    <link rel="stylesheet" href="dist/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="dist/css/magnific-popup.css">
    <link rel="stylesheet" href="dist/css/aos.css">
    <link rel="stylesheet" href="dist/css/ionicons.min.css">
    <link rel="stylesheet" href="dist/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="dist/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="dist/css/jquery.timepicker.css">
    <link rel="stylesheet" href="dist/css/flaticon.css">
    <link rel="stylesheet" href="dist/css/icomoon.css">
    <link rel="stylesheet" href="dist/css/style.css">
    <link rel="stylesheet" href="dist/css/style1.css">
    <link rel="stylesheet" href="dist/css/custom.css">

</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="#">UdR Reservation</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto" id="navcontroll">
                <li id="homenavi" class="nav-item "><a href="#home" class="nav-link page-scroll">Home</a></li>
                <li id="roomsnavi" class="nav-item"><a href="#services" class="nav-link page-scroll">Services</a></li>
                <li id="servicenavi" class="nav-item" ><a href="#rooms" class="nav-link page-scroll">Rooms</a></li>
                <li id="reservationnavi" class="nav-item" ><a href="#recervatin" class="nav-link page-scroll">Reservation</a></li>
                <!--<li id="aboutnavi" class="nav-item" ><a href="#" class="nav-link page-scroll">About Us</a></li>-->
                <li id="contactnavi" class="nav-item" ><a href="#contact" class="nav-link page-scroll">Contact</a></li>
            </ul>
        </div>

    </div>
</nav>

<!-- END nav -->

<div class="block-31" style="position: relative;" id="home">
    <div class="owl-carousel loop-block-31 ">
        <div class="block-30 item" style="background-image: url('dist/images/bg_2.jpg');" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-10">
                        <span class="subheading-sm">Welcome for UdR Reservation</span>
                        <h2 class="heading">Luxury Holiday</h2>
                        <!--<p><a href="#" class="btn btn-primary" style="padding: 15px">Learn More</a></p>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="block-30 item" style="background-image: url('dist/images/bg_1.jpg');" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-10">
                        <span class="subheading-sm">Welcome for UdR Reservation</span>
                        <h2 class="heading">Simple &amp; Elegant</h2>
                        <!--<p><a href="#" class="btn btn-primary" style="padding: 15px">Learn More</a></p>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="block-30 item" style="background-image: url('dist/images/bg_3.jpg');" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-10">
                        <span class="subheading-sm">Welcome for UdR Reservation</span>
                        <h2 class="heading">Food &amp; Drinks</h2>
                        <!--<p><a href="#" class="btn btn-primary" style="padding: 15px">Learn More</a></p>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">

    <div class="row site-section" id="services">
        <div class="col-md-12">
            <div class="row mb-5">
                <div class="col-md-7 section-heading">
                    <span class="subheading-sm">Facilities &amp; Services</span>
                    <!--<h2 class="heading">Facilities &amp; Services</h2>-->
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="media block-6">
                <div class="icon"><span class="flaticon-double-bed"></span></div>
                <div class="media-body">
                    <h3 class="heading">Luxury Rooms</h3>
                    <p>We provide the best and superb luxury rooms in country.it's will fell you better than better.</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="media block-6">
                <div class="icon"><span class="flaticon-wifi"></span></div>
                <div class="media-body">
                    <h3 class="heading">Fast &amp; Free Wifi</h3>
                    <p>No worry about your internet plan we can provide a fastest internet connection for you.</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="media block-6">
                <div class="icon"><span class="flaticon-customer-service"></span></div>
                <div class="media-body">
                    <h3 class="heading">Call Us 24/7</h3>
                    <p>The one and only hotel in the district who are open 24/7.it's will be more than comfortable for you.</p>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-lg-4">
            <div class="media block-6">
                <div class="icon"><span class="flaticon-taxi"></span></div>
                <div class="media-body">
                    <h3 class="heading">Travel Accomodation</h3>
                    <p>We give a support for your travelling with minimum cost.</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="media block-6">
                <div class="icon"><span class="flaticon-credit-card"></span></div>
                <div class="media-body">
                    <h3 class="heading">Accepts Credit Card</h3>
                    <p>Also we Accepts the credit card as your payment method there for you can pay your bill easily.</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="media block-6">
                <div class="icon"><span class="flaticon-dinner"></span></div>
                <div class="media-body">
                    <h3 class="heading">Restaurant</h3>
                    <p>Great food and drinks for your.it's fell you more hungry and thirsty.</p>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="site-section block-13 bg-light" id="rooms">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-7 section-heading">
                <span class="subheading-sm">Rooms &amp; Suites</span>
                <!--<h2 class="heading">Rooms &amp; Suites</h2>-->
                <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit, iusto, omnis! Quidem, sint, impedit? Dicta eaque delectus tempora hic, corporis velit doloremque quod quam laborum, nobis iusto autem culpa quaerat!</p>-->
            </div>
        </div>

        <div class="block-35">

            <ul class="nav" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-single" role="tab" aria-controls="pills-single" aria-selected="true">Single</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-double" role="tab" aria-controls="pills-double" aria-selected="false">Double</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-triple" role="tab" aria-controls="pills-triple" aria-selected="false">Triple</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-single" role="tabpanel" aria-labelledby="pills-single-tab">

                    <div class="row">
                        <div class="col-md-12 block-13">
                            <div class="nonloop-block-13 owl-carousel">
                                <div class="item">
                                    <div class="block-34">
                                        <div class="image">
                                            <a><img src="dist/images/img_1.jpg" alt="Image placeholder"></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="heading">Bachelor Room</h2>
                                            <div class="price"><sup>Rs</sup><span class="number" id="bp1">170</span><sub>/per night</sub></div>
                                            <ul class="specs" style="margin-top: 10px">
                                                <li id="bp2"><strong>Adults:</strong> 1</li>
                                                <li id="bp3"><strong>Categories:</strong> Single</li>
                                                <li id="bp4"><strong>Facilities:</strong> Closet with hangers, HD flat-screen TV, Telephone</li>
                                                <li id="bp5"><strong>Size:</strong> 20m<sup>2</sup></li>
                                                <li id="bp6"><strong>Bed Count:</strong> One bed</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="block-34">
                                        <div class="image">
                                            <a ><img src="dist/images/1.jpg" alt="Image placeholder"></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="heading">Couple Room</h2>
                                            <div class="price"><sup>Rs</sup><span class="number" id="bp7">170</span><sub>/per night</sub></div>
                                            <ul class="specs" style="margin-top: 10px">
                                                <li id="bp8"><strong>Adults:</strong> 2</li>
                                                <li id="bp9"><strong>Categories:</strong> Single</li>
                                                <li id="bp10"><strong>Facilities:</strong> Closet with hangers, HD flat-screen TV, Telephone</li>
                                                <li id="bp11"><strong>Size:</strong> 25m<sup>2</sup></li>
                                                <li id="bp12"><strong>Bed Count:</strong> One bed</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="block-34">
                                        <div class="image">
                                            <a ><img src="dist/images/img_3.jpg" alt="Image placeholder"></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="heading">Classic Room</h2>
                                            <div class="price"><sup>Rs</sup><span class="number" id="bp13">170</span><sub>/per night</sub></div>
                                            <ul class="specs" style="margin-top: 10px">
                                                <li id="bp14"><strong>Adults:</strong> 2</li>
                                                <li id="bp15"><strong>Categories:</strong> Single</li>
                                                <li id="bp16"><strong>Facilities:</strong> Closet with hangers, HD flat-screen TV, Telephone</li>
                                                <li id="bp17"><strong>Size:</strong> 30m<sup>2</sup></li>
                                                <li id="bp18"><strong>Bed Count:</strong> One bed</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>


                <div class="tab-pane fade" id="pills-double" role="tabpanel" aria-labelledby="pills-double-tab">
                    <div class="row">
                        <div class="col-md-12 block-13">
                            <div class="nonloop-block-13 owl-carousel">
                                <div class="item">
                                    <div class="block-34">
                                        <div class="image">
                                            <a><img src="dist/images/img_4.jpg" alt="Image placeholder"></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="heading">Double Room</h2>
                                            <div class="price"><sup>Rs</sup><span class="number" id="bp19">250</span><sub>/per night</sub></div>
                                            <ul class="specs" style="margin-top: 10px">
                                                <li id="bp20"><strong>Adults:</strong> 2</li>
                                                <li id="bp21"><strong>Categories:</strong> Double</li>
                                                <li id="bp22"><strong>Facilities:</strong> Closet with hangers, HD flat-screen TV, Telephone</li>
                                                <li id="bp23"><strong>Size:</strong> 25m<sup>2</sup></li>
                                                <li id="bp24"><strong>Bed Count:</strong> Two bed</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="block-34">
                                        <div class="image">
                                            <a><img src="dist/images/img_5.jpg" alt="Image placeholder"></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="heading">Family Room</h2>
                                            <div class="price"><sup>Rs</sup><span class="number" id="bp25">270</span><sub>/per night</sub></div>
                                            <ul class="specs" style="margin-top: 10px">
                                                <li id="bp26"><strong>Adults:</strong> 4</li>
                                                <li id="bp27"><strong>Categories:</strong> Double</li>
                                                <li id="bp28"><strong>Facilities:</strong> Closet with hangers, HD flat-screen TV, Telephone</li>
                                                <li id="bp29"><strong>Size:</strong> 27m<sup>2</sup></li>
                                                <li id="bp30"><strong>Bed Count:</strong> Two bed</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="block-34">
                                        <div class="image">
                                            <a><img src="dist/images/img_6.jpg" alt="Image placeholder"></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="heading">Presidential Room</h2>
                                            <div class="price"><sup>Rs</sup><span class="number" id="bp31">290</span><sub>/per night</sub></div>
                                            <ul class="specs" style="margin-top: 10px">
                                                <li id="bp32"><strong>Adults:</strong> 2</li>
                                                <li id="bp33"><strong>Categories:</strong> Double</li>
                                                <li id="bp34"><strong>Facilities:</strong> Closet with hangers, HD flat-screen TV, Telephone ,WIFI</li>
                                                <li id="bp35"><strong>Size:</strong> 25m<sup>2</sup></li>
                                                <li id="bp36"><strong>Bed Count:</strong> Two bed</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-triple" role="tabpanel" aria-labelledby="pills-triple-tab">
                    <div class="row">
                        <div class="col-md-12 block-13">
                            <div class="nonloop-block-13 owl-carousel">
                                <div class="item">
                                    <div class="block-34">
                                        <div class="image">
                                            <a><img src="dist/images/img_2.jpg" alt="Image placeholder"></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="heading">VIP Room</h2>
                                            <div class="price"><sup>Rs</sup><span class="number" id="bp37">350</span><sub>/per night</sub></div>
                                            <ul class="specs" style="margin-top: 10px">
                                                <li id="bp38"><strong>Adults:</strong> 1</li>
                                                <li id="bp39"><strong>Categories:</strong> Triple</li>
                                                <li id="bp40"><strong>Facilities:</strong> Closet with hangers, HD flat-screen TV, WIFI ,Breakfast</li>
                                                <li id="bp41"><strong>Size:</strong> 25m<sup>2</sup></li>
                                                <li id="bp42"><strong>Bed Count:</strong> One bed</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="site-section bg-light" id="recervatin" style="padding-top: 10px">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-7 section-heading">
                <span class="subheading-sm">Reserve Your Room</span>

            </div>
        </div>
        <form id="addReservationForm">
            <div class="row" >

                <div class="card card-primary col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6" >
                    <div class="card-header"><h4>Customer Details</h4></div>
                    <div class="card-body">
                        <form >
                            <div class="row">
                                <div class="form-group col-6 col-sm-8 col-md-5 col-lg-7 col-xl-7">
                                    <label>Customer</label>
                                    <input id="c1" name="custName" type="text" class="form-control" style="border-color: #cfcfcf">
                                </div>

                                <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <label>N.I.C</label>
                                    <input id="c2" name="nic" type="text" class="form-control" style="border-color: #cfcfcf">

                                </div>
                                <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <label>Tel</label>
                                    <input id="c3" name="tel" type="text" class="form-control" style="border-color: #cfcfcf">

                                </div>
                                <!--<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">-->
                                <!--<label>Credit Limit</label>-->
                                <!--<input type="text" class="form-control" style="border-color: #cfcfcf">-->

                                <!--</div>-->
                                <div class="col-md-6 mb-3 mb-lg-0 col-lg-6">
                                    <label class=" control-label">Check In</label>
                                    <div class="input-group date form_date " data-date="" data-date-format="yyyy.mm.dd">
                                        <input id="c4" class="form-control" size="16" type="text" value="" readonly name="checkIn">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3 mb-lg-0 col-lg-6">
                                    <label class=" control-label">Check Out</label>
                                    <div class="input-group date form_date " data-date="" data-date-format="yyyy.mm.dd" >
                                        <input id="c5" class="form-control" size="16" type="text" value="" readonly name="checkOut">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card card-primary col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="card-header"><h4>Room Details</h4></div>
                    <div class="card-body">

                        <form id="roomDetailsForm">
                            <div class="row">
                                <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <label>Room</label>
                                    <select id="roomNameList" name="roomNameList" class="form-control" style="border-color: #cfcfcf">

                                    </select>

                                </div>

                                <div class="form-group col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <label>Price</label>
                                    <input name="roomPrice" id="roomPrice" type="text" class="form-control" style="border-color: #cfcfcf" readonly>
                                </div>
                                <div class="form-group col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <label id="nlable">Available No of Rooms</label>
                                    <input name="noOfRooms" id="noOfRooms" type="text" class="form-control" style="border-color: #cfcfcf">

                                </div>
                                <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="text-align: center">
                                    <hr style="border-color: #cfcfcf">
                                    <label>Reservation Cost</label>
                                    <hr style="border-color: #cfcfcf">

                                </div>
                                <div class="form-group col-12">
                                    <button type="button" class="btn btn-primary " id="calculateTotal">Calculate Total</button>
                                </div>
                                <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <label>Total Price</label>
                                    <input name="totalPrice" id="totalPrice" type="text" class="form-control" style="border-color: #cfcfcf" readonly>

                                </div>
                                <div class="form-group col-12">
                                    <button id="makeRes" type="button" class="btn btn-primary " style="margin-top: 28px">Make Reservation</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </form>


    </div>
</div>





<footer class="footer" style="padding-bottom: 10px;padding-top: 10px" id="contact">
    <div class="container">
        <div class="row mb-5">

            <div class="col-md-6 col-lg-12">
                <div class="block-23">
                    <h3 class="heading-section">Contact Info</h3>
                    <ul>
                        <li><span class="icon icon-map-marker"></span><span class="text">Thalpitiya , Wadduva , Sri Lanka</span></li>
                        <li><a href="#"><span class="icon icon-phone"></span><span class="text">+9438-22 35 067</span></a></li>
                        <li><a href="#"><span class="icon icon-envelope"></span><span class="text">Info @ UdR Reservation.com</span></a></li>
                        <li><span class="icon icon-clock-o"></span><span class="text">Monday &mdash; Sunday 6:00am - 12:00pm</span></li>
                    </ul>
                </div>
            </div>


        </div>
        <div class="row pt-5" style="padding-bottom: 5px">
            <div class="col-md-12 text-left">
                <p>

                    Copyright &copy; All rights reserved  |  Made  by <a href="https://udaraprabathofficial.000webhostapp.com/" target="_blank" class="text-primary">UdR Creations</a>

                </p>
            </div>
        </div>
    </div>
</footer>

<script src="dist/js/jquery.min.js"></script>
<script src="dist/js/jquery-migrate-3.0.1.min.js"></script>
<script src="dist/js/popper.min.js"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script src="dist/js/jquery.easing.1.3.js"></script>
<script src="dist/js/jquery.waypoints.min.js"></script>
<script src="dist/js/jquery.stellar.min.js"></script>
<script src="dist/js/owl.carousel.min.js"></script>
<script src="dist/js/jquery.magnific-popup.min.js"></script>
<script src="dist/js/bootstrap-datepicker.js"></script>

<script src="dist/js/aos.js"></script>
<script src="dist/js/jquery.animateNumber.min.js"></script>
<script src="dist/js/main.js"></script>

<script type="text/javascript" src="dist/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="dist/js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script src="dist/controller/clientController.js"></script>
<script type="text/javascript">

    $('.form_date').datetimepicker({
        language:  'en',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

</script>

</body>
</html>