<?php
/**
 * Created by IntelliJ IDEA.
 * User: Udara Prabath
 * Date: 11/26/2018
 * Time: 10:17 PM
 */

class RoomReservation{
    private $resid;
    private $roomid;
    private $cust_Name;
    private $nic;
    private $phone;
    private $checkin;
    private $checkOut;
    private $no_of_room;
    private $total_price;

    /**
     * RoomReservation constructor.
     * @param $roomid
     * @param $cust_Name
     * @param $nic
     * @param $phone
     * @param $checkin
     * @param $checkOut
     * @param $no_of_room
     * @param $total_price
     */
    public function __construct($roomid, $cust_Name, $nic, $phone, $checkin, $checkOut, $no_of_room, $total_price)
    {
        $this->roomid = $roomid;
        $this->cust_Name = $cust_Name;
        $this->nic = $nic;
        $this->phone = $phone;
        $this->checkin = $checkin;
        $this->checkOut = $checkOut;
        $this->no_of_room = $no_of_room;
        $this->total_price = $total_price;
    }


    /**
     * @return mixed
     */
    public function getResid()
    {
        return $this->resid;
    }

    /**
     * @param mixed $resid
     */
    public function setResid($resid): void
    {
        $this->resid = $resid;
    }

    /**
     * @return mixed
     */
    public function getRoomid()
    {
        return $this->roomid;
    }

    /**
     * @param mixed $roomid
     */
    public function setRoomid($roomid): void
    {
        $this->roomid = $roomid;
    }

    /**
     * @return mixed
     */
    public function getCustName()
    {
        return $this->cust_Name;
    }

    /**
     * @param mixed $cust_Name
     */
    public function setCustName($cust_Name): void
    {
        $this->cust_Name = $cust_Name;
    }

    /**
     * @return mixed
     */
    public function getNic()
    {
        return $this->nic;
    }

    /**
     * @param mixed $nic
     */
    public function setNic($nic): void
    {
        $this->nic = $nic;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getCheckin()
    {
        return $this->checkin;
    }

    /**
     * @param mixed $checkin
     */
    public function setCheckin($checkin): void
    {
        $this->checkin = $checkin;
    }

    /**
     * @return mixed
     */
    public function getCheckOut()
    {
        return $this->checkOut;
    }

    /**
     * @param mixed $checkOut
     */
    public function setCheckOut($checkOut): void
    {
        $this->checkOut = $checkOut;
    }

    /**
     * @return mixed
     */
    public function getNoOfRoom()
    {
        return $this->no_of_room;
    }

    /**
     * @param mixed $no_of_room
     */
    public function setNoOfRoom($no_of_room): void
    {
        $this->no_of_room = $no_of_room;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->total_price;
    }

    /**
     * @param mixed $total_price
     */
    public function setTotalPrice($total_price): void
    {
        $this->total_price = $total_price;
    }


}