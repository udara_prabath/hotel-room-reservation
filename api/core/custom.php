<?php
/**
 * Created by IntelliJ IDEA.
 * User: Udara Prabath
 * Date: 11/27/2018
 * Time: 11:48 AM
 */

class custom{

    private $category;
    private $price;
    private $adults;
    private $bedSize;
    private $bedCount;
    private $totalRooms;



    /**
     * custom constructor.
     * @param $category
     * @param $price
     * @param $adults
     * @param $bedSize
     * @param $bedCount
     * @param $totalRooms
     */

    public function __construct($category, $price, $adults, $bedSize, $bedCount, $totalRooms)
    {
        $this->category = $category;
        $this->price = $price;
        $this->adults = $adults;
        $this->bedSize = $bedSize;
        $this->bedCount = $bedCount;
        $this->totalRooms = $totalRooms;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getAdults()
    {
        return $this->adults;
    }

    /**
     * @param mixed $adults
     */
    public function setAdults($adults): void
    {
        $this->adults = $adults;
    }

    /**
     * @return mixed
     */
    public function getBedSize()
    {
        return $this->bedSize;
    }

    /**
     * @param mixed $bedSize
     */
    public function setBedSize($bedSize): void
    {
        $this->bedSize = $bedSize;
    }

    /**
     * @return mixed
     */
    public function getBedCount()
    {
        return $this->bedCount;
    }

    /**
     * @param mixed $bedCount
     */
    public function setBedCount($bedCount): void
    {
        $this->bedCount = $bedCount;
    }

    /**
     * @return mixed
     */
    public function getTotalRooms()
    {
        return $this->totalRooms;
    }

    /**
     * @param mixed $totalRooms
     */
    public function setTotalRooms($totalRooms): void
    {
        $this->totalRooms = $totalRooms;
    }



}