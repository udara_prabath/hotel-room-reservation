<?php
/**
 * Created by IntelliJ IDEA.
 * User: Udara Prabath
 * Date: 11/27/2018
 * Time: 8:21 PM
 */

class cat{
    private static $room;
    private static $count;
    private static $total;

    /**
     * cat constructor.
     */
    public function __construct()
    {
    }


    /**
     * @return mixed
     */
    public static function getRoom()
    {
        return self::$room;
    }

    /**
     * @param mixed $room
     */
    public static function setRoom($room): void
    {
        self::$room = $room;
    }

    /**
     * @return mixed
     */
    public static function getCount()
    {
        return self::$count;
    }

    /**
     * @param mixed $count
     */
    public static function setCount($count): void
    {
        self::$count = $count;
    }

    /**
     * @return mixed
     */
    public static function getTotal()
    {
        return self::$total;
    }

    /**
     * @param mixed $total
     */
    public static function setTotal($total): void
    {
        self::$total = $total;
    }


}