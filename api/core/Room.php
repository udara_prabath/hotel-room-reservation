<?php
/**
 * Created by IntelliJ IDEA.
 * User: sanu-vithanage
 * Date: 11/23/18
 * Time: 2:26 PM
 */


class Room{
    private $roomid;
    private $room_Type;
    private $room_category;
    private $price;
    private $adults;
    private $bedSize;
    private $no_of_bed;
    private $total_rooms;
    private $available_room;
    private $booked_room;

    /**
     * Room constructor.
     * @param $roomid
     * @param $room_Type
     * @param $room_category
     * @param $price
     * @param $adults
     * @param $bedSize
     * @param $no_of_bed
     * @param $total_rooms
     * @param $available_room
     * @param $booked_room
     */
    public function __construct($roomid, $room_Type, $room_category, $price, $adults, $bedSize, $no_of_bed, $total_rooms, $available_room, $booked_room)
    {
        $this->roomid = $roomid;
        $this->room_Type = $room_Type;
        $this->room_category = $room_category;
        $this->price = $price;
        $this->adults = $adults;
        $this->bedSize = $bedSize;
        $this->no_of_bed = $no_of_bed;
        $this->total_rooms = $total_rooms;
        $this->available_room = $available_room;
        $this->booked_room = $booked_room;
    }

    /**
     * @return mixed
     */
    public function getRoomid()
    {
        return $this->roomid;
    }

    /**
     * @param mixed $roomid
     */
    public function setRoomid($roomid): void
    {
        $this->roomid = $roomid;
    }

    /**
     * @return mixed
     */
    public function getRoomType()
    {
        return $this->room_Type;
    }

    /**
     * @param mixed $room_Type
     */
    public function setRoomType($room_Type): void
    {
        $this->room_Type = $room_Type;
    }

    /**
     * @return mixed
     */
    public function getRoomCategory()
    {
        return $this->room_category;
    }

    /**
     * @param mixed $room_category
     */
    public function setRoomCategory($room_category): void
    {
        $this->room_category = $room_category;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getAdults()
    {
        return $this->adults;
    }

    /**
     * @param mixed $adults
     */
    public function setAdults($adults): void
    {
        $this->adults = $adults;
    }

    /**
     * @return mixed
     */
    public function getBedSize()
    {
        return $this->bedSize;
    }

    /**
     * @param mixed $bedSize
     */
    public function setBedSize($bedSize): void
    {
        $this->bedSize = $bedSize;
    }

    /**
     * @return mixed
     */
    public function getNoOfBed()
    {
        return $this->no_of_bed;
    }

    /**
     * @param mixed $no_of_bed
     */
    public function setNoOfBed($no_of_bed): void
    {
        $this->no_of_bed = $no_of_bed;
    }

    /**
     * @return mixed
     */
    public function getTotalRooms()
    {
        return $this->total_rooms;
    }

    /**
     * @param mixed $total_rooms
     */
    public function setTotalRooms($total_rooms): void
    {
        $this->total_rooms = $total_rooms;
    }

    /**
     * @return mixed
     */
    public function getAvailableRoom()
    {
        return $this->available_room;
    }

    /**
     * @param mixed $available_room
     */
    public function setAvailableRoom($available_room): void
    {
        $this->available_room = $available_room;
    }

    /**
     * @return mixed
     */
    public function getBookedRoom()
    {
        return $this->booked_room;
    }

    /**
     * @param mixed $booked_room
     */
    public function setBookedRoom($booked_room): void
    {
        $this->booked_room = $booked_room;
    }



}