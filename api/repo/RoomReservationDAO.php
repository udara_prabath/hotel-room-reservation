<?php
/**
 * Created by IntelliJ IDEA.
 * User: Udara Prabath
 * Date: 11/26/2018
 * Time: 10:33 PM
 */

interface RoomReservationDAO{
    public function setConnection(mysqli $connection);
    public function addRoomReservation(RoomReservation $room):bool ;
    public function updateRoomReservation(RoomReservation $room):bool ;
    public function searchRoomReservation($nic,$date):array ;
    public function getAllReservation():array;
}