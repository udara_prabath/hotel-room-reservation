<?php
/**
 * Created by IntelliJ IDEA.
 * User: Udara Prabath
 * Date: 11/26/2018
 * Time: 10:35 PM
 */

require_once __DIR__.'/../RoomReservationDAO.php';
require_once __DIR__.'/../../db/DBConnection.php';
require_once __DIR__.'/../../core/RoomReservation.php';

class RoomReservationDAOImpl implements RoomReservationDAO{

    private $connection;

    public function setConnection(mysqli $connection){
        $this->connection = $connection;
    }

    public function addRoomReservation(RoomReservation $room): bool{
        $custName=$room->getCustName();
        $NIC=$room->getNic();
        $tel=$room->getPhone();
        $dateIn=$room->getCheckin();
        $dateOut=$room->getCheckOut();
        $roomCategory=$room->getRoomid();
        $noOfRooms=$room->getNoOfRoom();
        $totalPrice=$room->getTotalPrice();
        $result=  $this->connection->query("call makeReservation('$roomCategory','$custName','$NIC','$tel','$dateIn','$dateOut','$noOfRooms','$totalPrice')");
        $row=mysqli_fetch_row($result);
        if ($row[0]>0){
            return true;
        }
        return false;

    }

    public function updateRoomReservation(RoomReservation $room): bool{
        return false;
    }

    public function searchRoomReservation($nic,$date): array {
        $n=$nic;
        $d=$date;
        $result=  $this->connection->query("select r.room_category,c.cust_name,c.nic,c.no_of_room,c.checkIn,c.checkOut,c.total_price from room r,reservation c where c.roomid=r.roomid and c.nic='$nic' and c.checkin='$date'");
        $row=mysqli_fetch_row($result);
        return $row;
    }

    public function getAllReservation(): array{
        $result=  $this->connection->query(" select r.room_category,c.cust_name,c.nic,c.no_of_room,c.checkIn,c.checkOut,c.total_price from room r,reservation c where c.roomid=r.roomid ");
        return $result->fetch_all();
    }
}