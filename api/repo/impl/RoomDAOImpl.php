<?php
/**
 * Created by IntelliJ IDEA.
 * User: Udara Prabath
 * Date: 11/26/2018
 * Time: 10:34 PM
 */

require_once __DIR__.'/../RoomDAO.php';
require_once __DIR__.'/../../db/DBConnection.php';
require_once __DIR__.'/../../core/Room.php';
require_once __DIR__ . '/../../core/custom.php';
class RoomDAOImpl implements RoomDAO{

    private $connection;

    public function setConnection(mysqli $connection){
        $this->connection = $connection;
    }

    public function addRoom(Room $room): bool{
        return false;
    }

    public function updateRoom(custom $dto): bool{
        $result=  $this->connection->query("UPDATE room set price='{$dto->getPrice()}',adults='{$dto->getAdults()}',bedSize='{$dto->getBedSize()}',no_of_bed='{$dto->getBedCount()}',total_rooms='{$dto->getTotalRooms()}' WHERE room_category='{$dto->getCategory()}'");
        return $result>0;
    }

    public function searchRoom($roomCategory): array {
        $c=$roomCategory;
        $result=  $this->connection->query("SELECT * from room where room_category='$c'");
        return $result->fetch_row();
    }

    public function getAllRoom(): array{
        $result=  $this->connection->query("Select * from room");
        return $result->fetch_all();
    }
    public function getAllRoomName():array{
        $result=  $this->connection->query("Select room_category from room");
        return $result->fetch_all();
    }
    public function getAllRoomPrice($name):array{
        $n=$name;
        $result=  $this->connection->query("Select price,available_room from room where room_category='$n'");
        $row=mysqli_fetch_row($result);
        return $row;
    }
    public function getDateDiff($checkIN,$checkOUT):array{
        $in=$checkIN;
        $out=$checkOUT;
        $result=  $this->connection->query("Select dateDiff('$out','$in')");
        $row=mysqli_fetch_row($result);
        return $row;
    }
}