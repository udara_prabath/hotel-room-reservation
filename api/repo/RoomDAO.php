<?php
/**
 * Created by IntelliJ IDEA.
 * User: Udara Prabath
 * Date: 11/26/2018
 * Time: 10:33 PM
 */

interface RoomDAO{
    public function setConnection(mysqli $connection);
    public function addRoom(Room $room):bool ;
    public function updateRoom(custom $room):bool ;
    public function searchRoom($roomCategory):array;
    public function getAllRoom():array;
    public function getAllRoomName():array;
    public function getAllRoomPrice($name):array;
    public function getDateDiff($checkIN,$checkOUT):array;
}