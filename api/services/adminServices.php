<?php

require_once __DIR__ . '/../business/impl/RoomBOImpl.php';
require_once __DIR__ . '/../business/impl/RoomReservationBOImpl.php';
require_once __DIR__ . '/../core/Room.php';
require_once __DIR__ . '/../core/RoomReservation.php';
require_once __DIR__ . '/../core/custom.php';
require_once __DIR__ . '/../core/Cat.php';
$roomBO=new RoomBOImpl();
$roomRes=new RoomReservationBOImpl();
$method= $_SERVER['REQUEST_METHOD'];

switch ($method){
    case "POST":

    break;

    case "GET":
        $operation = $_GET['operation'];

        switch ($operation){
            case "getAll";
                echo json_encode($roomBO->getAllRoom());
                break;
            case "getAllRoomNames";
                echo json_encode($roomBO->getAllRoomName());
                break;
            case "getAllRoomPrice";
                $price2=$_GET['roomNameList'];
                Cat::setRoom($price2);
                echo json_encode($roomBO->getAllRoomPrice($price2));
                break;
            case "search";
                $roomcat= $_GET['room'];
                echo json_encode($roomBO->searchRoom($roomcat));
                break;
            case "update";
                 $category = $_GET['category'];
                 $price = $_GET['price'];
                 $adults = $_GET['adultsCount'];
                 $bedSize = $_GET['bedSize'];
                 $bedCount = $_GET['bedCount'];
                 $totalRooms = $_GET['totalRooms'];
                echo json_encode($roomBO->updateRoom(new custom($category,$price,$adults,$bedSize,$bedCount,$totalRooms)));
                break;
            case "CalculateTotal";
                $checkIN =$_GET['checkIn'];
                $checkOUT =$_GET['checkOut'];
                echo json_encode($roomBO->getDateDiff($checkIN,$checkOUT));
                break;
            case "AddReservation";
                $custName=$_GET['custName'];
                $NIC=$_GET['nic'];
                $tel=$_GET['tel'];
                $dateIn=$_GET['checkIn'];
                $dateOut=$_GET['checkOut'];
                $roomCategory=$_GET['room'];
                $noOfRooms=$_GET['no'];
                $totalPrice=$_GET['pr'];
                echo $custName.$NIC.$tel.$dateIn.$dateOut.Cat::getRoom().Cat::getCount().Cat::getTotal();
//                echo $roomRes->addRoomReservation(new RoomReservation('Bachelor Room','a','a','1','2018.11.27','2018.11.28','2','25000'));
//                echo $roomRes->addRoomReservation(new RoomReservation($roomCategory,$custName,$NIC,$tel,$dateIn,$dateOut,$noOfRooms,$totalPrice));
                break;
            case "getAllReservation";
                echo json_encode($roomRes->getAllReservation());
                break;
            case "SearchReservation";
                $ck=$_GET['checkIn2'];
                $nc=$_GET['N.I.C2'];
                echo json_encode($roomRes->searchRoomReservation($nc,$ck));
                break;
            case "saveTotal";
                $noOf=$_GET['noOfRooms'];
                $total=$_GET['totalPrice'];

                Cat::setTotal($noOf);
                Cat::setCount($total);

               // echo $noOf.$total;
               //Cat::getCount().
               echo  Cat::getTotal().Cat::getRoom();
        }

    break;
}
