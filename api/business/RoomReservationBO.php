<?php
/**
 * Created by IntelliJ IDEA.
 * User: Udara Prabath
 * Date: 11/26/2018
 * Time: 10:26 PM
 */

interface RoomReservationBO{
    public function addRoomReservation(RoomReservation $room):bool ;
    public function updateRoomReservation(RoomReservation $room):bool ;
    public function searchRoomReservation($nic,$dateIn):array ;
    public function getAllReservation():array;

}