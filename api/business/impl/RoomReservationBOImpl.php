<?php
/**
 * Created by IntelliJ IDEA.
 * User: Udara Prabath
 * Date: 11/26/2018
 * Time: 10:29 PM
 */
require_once __DIR__ . '/../RoomReservationBO.php';
require_once __DIR__ . '/../../core/RoomReservation.php';
require_once __DIR__ . '/../../db/DBConnection.php';
require_once __DIR__ . '/../../repo/impl/RoomReservationDAOImpl.php';


class RoomReservationBOImpl implements RoomReservationBO {

    private $roomReservationDAO;

    /**
     * RoomReservationBOImpl constructor.
     */
    public function __construct(){
        $this->roomReservationDAO=new RoomReservationDAOImpl();
    }


    public function addRoomReservation(RoomReservation $room): bool{
        $connection = (new DBConnection())->getConnection();
        $this->roomReservationDAO->setConnection($connection);
        return $this->roomReservationDAO->addRoomReservation($room);
    }

    public function updateRoomReservation(RoomReservation $room): bool{
        $connection = (new DBConnection())->getConnection();
        $this->roomReservationDAO->setConnection($connection);
        return $this->roomReservationDAO->updateRoomReservation($room);
    }

    public function searchRoomReservation($nic,$dateIn): array {
        $connection = (new DBConnection())->getConnection();
        $this->roomReservationDAO->setConnection($connection);
        return $this->roomReservationDAO->searchRoomReservation($nic,$dateIn);
    }

    public function getAllReservation(): array{
        $connection = (new DBConnection())->getConnection();
        $this->roomReservationDAO->setConnection($connection);
        return $this->roomReservationDAO->getAllReservation();
    }
}