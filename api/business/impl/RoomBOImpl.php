<?php
/**
 * Created by IntelliJ IDEA.
 * User: Udara Prabath
 * Date: 11/26/2018
 * Time: 10:29 PM
 */
require_once __DIR__ . '/../RoomBO.php';
require_once __DIR__ . '/../../core/Room.php';
require_once __DIR__ . '/../../core/custom.php';
require_once __DIR__ . '/../../db/DBConnection.php';
require_once __DIR__ . '/../../repo/impl/RoomDAOImpl.php';


class RoomBOImpl implements RoomBO {

    private $roomRepo;

    /**
     * RoomBOImpl constructor.
     */
    public function __construct(){
        $this->roomRepo = new RoomDAOImpl();
    }


    public function addRoom(Room $room): bool{
        $connection = (new DBConnection())->getConnection();
        $this->roomRepo->setConnection($connection);
        return $this->roomRepo->addRoom($room);
    }

    public function updateRoom(custom $room): bool{
        $connection = (new DBConnection())->getConnection();
        $this->roomRepo->setConnection($connection);
        return $this->roomRepo->updateRoom($room);
    }

    public function searchRoom($roomCategory): array{
        $connection = (new DBConnection())->getConnection();
        $this->roomRepo->setConnection($connection);
        return $this->roomRepo->searchRoom($roomCategory);
    }

    public function getAllRoom(): array{
        $connection = (new DBConnection())->getConnection();
        $this->roomRepo->setConnection($connection);
        return $this->roomRepo->getAllRoom();
    }

    public function getAllRoomName():array {
        $connection = (new DBConnection())->getConnection();
        $this->roomRepo->setConnection($connection);
        return $this->roomRepo->getAllRoomName();
    }
    public function getAllRoomPrice($name):array{
        $connection = (new DBConnection())->getConnection();
        $this->roomRepo->setConnection($connection);
        return $this->roomRepo->getAllRoomPrice($name);
    }
    public function getDateDiff($checkIN,$checkOUT):array{
        $connection = (new DBConnection())->getConnection();
        $this->roomRepo->setConnection($connection);
        return $this->roomRepo->getDateDiff($checkIN,$checkOUT);
    }
}