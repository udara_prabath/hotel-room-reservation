<?php
session_start();
//session_unset();
//session_destroy();

    if(!isset($_SESSION['login'])){
        header("Location:index.php");
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
    <title>UdR Reservation</title>

    <link rel="stylesheet" href="dist/Admin/dist/modules/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="dist/Admin/dist/modules/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="dist/Admin/dist/modules/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css">

    <link rel="stylesheet" href="dist/Admin/dist/modules/summernote/summernote-lite.css">
    <link rel="stylesheet" href="dist/Admin/dist/modules/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="dist/Admin/dist/css/demo.css">
    <link rel="stylesheet" href="dist/Admin/dist/css/style.css">
    <link rel="stylesheet" href="dist/css/bootstrap-datetimepicker.min.css">
</head>
<style>
    /*#DashBoardContent{*/
    /*display: none;*/
    /*}*/
    /*#CustomerContent{*/
    /*display: none;*/
    /*}*/
    /*#OrderContent{*/
    /*display: none;*/
    /*}*/
    #ReservationContent{
        display: none;
    }
</style>

<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <nav class="navbar navbar-expand-lg main-navbar">
            <form class="form-inline mr-auto">
                <ul class="navbar-nav mr-3">
                    <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="ion ion-navicon-round"></i></a></li>
                    <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="ion ion-search"></i></a></li>
                </ul>
                <div class="search-element">
                    <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn" type="submit"><i class="ion ion-search"></i></button>
                </div>
            </form>
            <ul class="navbar-nav navbar-right">

                <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg">
                        <i class="ion ion-android-person d-lg-none"></i>
                        <div class="d-sm-none d-lg-inline-block">Hi, Udara</div></a>
                    <div class="dropdown-menu dropdown-menu-right">

                        <a href="#" class="dropdown-item has-icon" id="LogOut">
                            <i class="ion ion-log-out" ></i> Logout
                        </a>
                    </div>
                </li>
            </ul>
        </nav>
        <div class="main-sidebar">
            <aside id="sidebar-wrapper">
                <div class="sidebar-brand">
                    <a href="#">UdR Reservation</a>
                </div>
                <div class="sidebar-user">
                    <div class="sidebar-user-picture">
                        <img alt="image" src="dist/Admin/dist/img/avatar/avatar-1.jpeg">
                    </div>
                    <div class="sidebar-user-details">
                        <div class="user-name">UDARA PRABATH</div>
                        <div class="user-role">
                            Administrator
                        </div>
                    </div>
                </div>
                <ul class="sidebar-menu">

                    <li class="menu-header">Menu</li>
                    <li ><a href="#" id="Roombtn" style="color: #0b0b0b"><i class="ion ion-ios-list-outline"></i><span id="rText">Room Details</span></a></li>
                    <li><a href="#" id="Reservationbtn"><i class="ion ion-clipboard"></i><span id="rsText">Reservation Details</span></a></li>
                    <!--<li><a href="#" id="Ordesbtn"><i class="ion ion-ios-list-outline"></i><span>Orders</span></a></li>-->
                    <!--<li><a href="#"><i class="ion ion-ios-people"></i><span>Employees</span></a></li>-->

                </ul>

            </aside>
        </div>








    </div>
    <div class="main-content" id="RoomContent">
        <section class="section">
            <h1 class="section-header">
                <div>Room Details</div>
            </h1>
            <div class="row">
                <div class="btn-group col-12 col-sm-10 col-md-8 col-lg-3" role="group" aria-label="Basic example" style="padding: 20px;">
                    <button type="button" class="btn btn-secondary col-12 col-sm-10 col-md-8 col-lg-12" id="viewAllRooms">View All Rooms</button>
                    <button type="button" class="btn btn-secondary col-12 col-sm-10 col-md-8 col-lg-12" data-toggle="modal" data-target="#SearchRoom">Search Room</button>
                </div>
            </div>


            <div class="row">
                <div class="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Room Details</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Room Type</th>
                                            <th>Room Category</th>
                                            <th>Total Rooms</th>
                                            <th>Available Rooms</th>
                                            <th>Booked</th>
                                        </tr>
                                    </thead>
                                    <tbody id="roomTableBody">

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </section>
    </div>
    <div class="main-content" id="ReservationContent">
        <section class="section">
            <h1 class="section-header">
                <div>Reservation Details</div>
            </h1>
            <div class="row">
                <div class="btn-group col-12 col-sm-10 col-md-8 col-lg-3" role="group" aria-label="Basic example" style="padding: 20px;">
                    <button type="button" class="btn btn-secondary col-12 col-sm-10 col-md-8 col-lg-12" data-toggle="modal" data-target="#MakeReservation">New Reservation</button>
                    <button id="getAllReservation" type="button" class="btn btn-secondary col-12 col-sm-10 col-md-8 col-lg-12">View All Reservations</button>

                </div>
            </div>
            <div class="row">
                <div class="btn-group col-12 col-sm-10 col-md-8 col-lg-3" role="group" aria-label="Basic example" style="padding: 20px;">

                    <button type="button" class="btn btn-secondary col-12 col-sm-10 col-md-8 col-lg-12" data-toggle="modal" data-target="#SearchReservation">Search Reservations</button>
                </div>
            </div>


            <div class="row">
                <div class="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Reservation Details</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th>Room Category</th>
                                            <th>Customer Name</th>
                                            <th>N.I.C</th>
                                            <th>No of Rooms</th>
                                            <th>Check In</th>
                                            <th>Check Out</th>
                                            <th>Price</th>

                                        </tr>
                                    </tbody>
                                    <tbody id="ReservationTable">

                                    </tbody>


                                </table>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </section>
    </div>

    <!--====================================-->
    <div class="modal fade" id="SearchRoom" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" id="mode">
                <!--<div class="modal-header bg-primary">-->

                <!--<h4 class="modal-title card-header" style="color: white"><b>Search Room</b></h4>-->
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <div class="card card-primary">
                    <div class="card-header"><h4>Search Room</h4></div>
                    <div class="card-body">

                            <div class="row">
                                <div class="form-group col-12">
                                    <form id="searchRoom1">
                                        <label>Room Category</label>
                                        <input id="rname" type="text" class="form-control" name="room" autofocus>
                                        <button type="button" class="btn btn-secondary" id="searchRoombtn">Search Room</button>
                                    </form>
                                </div>

                            </div>

                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group ">
                        <form id="RoomSearchResult">
                            <label>Category</label>
                            <input id="category" name="category" type="text" class="form-control">

                            <label>Price</label>

                            <input id="price" name="price" type="text" class="form-control">


                            <label>Adults Count</label>
                            <input id="adultsCount" name="adultsCount" type="text" class="form-control">

                            <label>Bed Size</label>
                            <input id="bedSize" name="bedSize" type="text" class="form-control">


                            <label>Bed Count</label>
                            <input id="bedCount" name="bedCount" type="text" class="form-control">

                            <label>Total Rooms</label>
                            <input id="totalRooms" name="totalRooms" type="text"  class="form-control">
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button"  class="btn btn-success" id="updateRoom"><b>Save</b></button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><b>Close</b></button>

                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="SearchReservation" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" id="mode1">

                <div class="card card-primary">
                    <div class="card-header"><h4>Search Reservation</h4></div>
                    <div class="card-body">
                        <form id="searchReservationForm">
                            <div class="row">

                                <div class="form-group col-6">
                                    <label for="frist_name2">N.I.C</label>
                                    <input id="frist_name2" type="text" class="form-control" name="N.I.C2">
                                </div>
                                <div class="col-6">
                                    <label class=" control-label">Check In</label>
                                    <div class="input-group date form_date " data-date="" data-date-format="yyyy.mm.dd">
                                        <input class="form-control" size="16" type="text" value="" readonly name="checkIn2">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>

                            </div>

                            <div class="btn-group col-2">
                                <button type="button" class="btn btn-secondary" id="searchReservationbtn">Search Reservation</button>

                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group ">
                        <form id="searchReservationResultForm">
                        <!--<label for="name">Name :</label>-->
                            <label>Room Category</label>

                            <input id="category" type="text" class="form-control">
                            <label>Customer</label>

                            <input id="customer" type="text" class="form-control">

                            <!--<label for="name">Age :</label>-->
                            <label>N.I.C</label>
                            <input id="nic" type="text" class="form-control">

                            <label>Room Count</label>
                            <input id="roomCount" type="text" class="form-control">

                            <!--<label for="name">Tp :</label>-->
                            <label>CheckIn</label>
                            <input id="checkIn0" type="text" class="form-control">
                            <!--<label for="name">Salary :</label>-->
                            <label>CheckOut</label>
                            <input id="checkOut0" type="text" class="form-control">
                            <label>Price</label>
                            <input id="price2" type="text" class="form-control">
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button"  class="btn btn-success"><b>Save</b></button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><b>Close</b></button>

                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="MakeReservation" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" id="mode2">


                <div class="modal-body">
                    <div class="row" >
                        <form id="addReservationForm">
                            <div class="card card-primary col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" >
                                <div class="card-header"><h4>Customer Details</h4></div>
                                <div class="card-body">
                                    <form >
                                        <div class="row">
                                            <div class="form-group col-6 col-sm-8 col-md-5 col-lg-7 col-xl-7">
                                                <label>Customer</label>
                                                <input id="c1" name="custName" type="text" class="form-control" style="border-color: #cfcfcf">
                                            </div>

                                            <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                <label>N.I.C</label>
                                                <input id="c2" name="nic" type="text" class="form-control" style="border-color: #cfcfcf">

                                            </div>
                                            <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                <label>Tel</label>
                                                <input id="c3" name="tel" type="text" class="form-control" style="border-color: #cfcfcf">

                                            </div>
                                            <!--<div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">-->
                                            <!--<label>Credit Limit</label>-->
                                            <!--<input type="text" class="form-control" style="border-color: #cfcfcf">-->

                                            <!--</div>-->
                                            <div class="col-md-6 mb-3 mb-lg-0 col-lg-6">
                                                <label class=" control-label">Check In</label>
                                                <div class="input-group date form_date " data-date="" data-date-format="yyyy.mm.dd">
                                                    <input id="c4" class="form-control" size="16" type="text" value="" readonly name="checkIn">
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-3 mb-lg-0 col-lg-6">
                                                <label class=" control-label">Check Out</label>
                                                <div class="input-group date form_date " data-date="" data-date-format="yyyy.mm.dd" >
                                                    <input id="c5" class="form-control" size="16" type="text" value="" readonly name="checkOut">
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="card card-primary col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="card-header"><h4>Room Details</h4></div>
                                    <div class="card-body">

                                    <form id="roomDetailsForm">
                                        <div class="row">
                                            <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                <label>Room</label>
                                                <select id="roomNameList" name="roomNameList" class="form-control" style="border-color: #cfcfcf">

                                                </select>

                                            </div>

                                            <div class="form-group col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                <label>Price</label>
                                                <input name="roomPrice" id="roomPrice" type="text" class="form-control" style="border-color: #cfcfcf" readonly>
                                            </div>
                                            <div class="form-group col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                <label id="nlable">Available No of Rooms</label>
                                                <input name="noOfRooms" id="noOfRooms" type="text" class="form-control" style="border-color: #cfcfcf">

                                            </div>
                                            <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="text-align: center">
                                                <hr style="border-color: #cfcfcf">
                                                <label>Reservation Cost</label>
                                                <hr style="border-color: #cfcfcf">

                                            </div>
                                            <div class="form-group col-12">
                                                <button type="button" class="btn btn-primary " id="calculateTotal">Calculate Total</button>
                                            </div>
                                            <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                <label>Total Price</label>
                                                <input name="totalPrice" id="totalPrice" type="text" class="form-control" style="border-color: #cfcfcf" readonly>

                                            </div>
                                            <div class="form-group col-12">
                                                <button id="makeRes" type="button" class="btn btn-primary " style="margin-top: 28px">Make Reservation</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--<button type="button"  class="btn btn-success"><b>Save</b></button>-->
                    <!--<button type="button" class="btn btn-danger" data-dismiss="modal"><b>Close</b></button>-->

                </div>
            </div>

        </div>
    </div>



    <footer class="main-footer">
        <div class="footer-left">
            Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="#">UdR Creations</a>
        </div>
        <div class="footer-right"></div>
    </footer>
</div>


<script src="dist/Admin/dist/modules/jquery.min.js"></script>
<script src="dist/Admin/dist/modules/popper.js"></script>
<script src="dist/Admin/dist/modules/tooltip.js"></script>
<script src="dist/Admin/dist/modules/bootstrap/js/bootstrap.min.js"></script>
<script src="dist/Admin/dist/modules/nicescroll/jquery.nicescroll.min.js"></script>
<script src="dist/Admin/dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js"></script>
<script src="dist/Admin/dist/js/sa-functions.js"></script>

<script src="dist/Admin/dist/modules/chart.min.js"></script>
<script src="dist/Admin/dist/modules/summernote/summernote-lite.js"></script>
<script>


    $('#Roombtn').click(function () {
        $('#ReservationContent').css('display','none');
        $('#RoomContent').css('display','inherit');
        $('#Roombtn').css('color','black');
        $('#Reservationbtn').css('color','#868e96');
    });
    $('#Reservationbtn').click(function () {
        $('#ReservationContent').css('display','inherit');
        $('#RoomContent').css('display','none');
        $('#Roombtn').css('color','#868e96');
        $('#Reservationbtn').css('color','black');
    });



</script>

<script src="dist/Admin/dist/js/scripts.js"></script>
<script src="dist/Admin/dist/js/custom.js"></script>
<script src="dist/Admin/dist/js/demo.js"></script>
<script src="dist/controller/adminController.js"></script>
<script type="text/javascript" src="dist/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="dist/js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script type="text/javascript">

    $('.form_date').datetimepicker({
        language:  'en',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

</script>
</body>
</html>




