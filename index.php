<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="dist/css/loginCSS.css">
</head>
<body>
<div class="login-wrap">
    <div class="login-html">
        <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Sign In</label>
        <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Sign Up</label>

            <div class="login-form">
                <div class="sign-in-htm">
                    <form class="group" action="loginController.php" method="GET">

                            <label style="margin-top: 10px"  class="label">Username</label>
                            <input style="margin-top: 10px" id="user1" name="userName1" type="text" class="input">

                            <label style="margin-top: 20px"  class="label">Password</label>
                            <input style="margin-top: 10px" id="pass1" name="passWord1" type="password" class="input" data-type="password">

                            <input style="margin-top: 20px" id="check" type="checkbox" class="check" checked>
                            <label style="margin-top: 10px" for="check"><span class="icon"></span> Keep me Signed in</label>

                            <input type="submit" class="button" value="Sign In" style="margin-top: 50px">

                            <div class="hr"></div>

                            <a href="#">Forgot Password?</a>

                    </form>
                </div>
                <div class="sign-up-htm">
                    <form class="group" action="loginController.php" method="get">

                            <label style="margin-top: 10px" for="user" class="label">Username</label>
                            <input style="margin-top: 4px" id="user" type="text" class="input">

                            <label style="margin-top: 10px" for="pass2" class="label">Password</label>
                            <input style="margin-top: 4px" id="pass2" type="password" class="input" data-type="password">

                            <label style="margin-top: 10px" for="pass3" class="label">Repeat Password</label>
                            <input style="margin-top: 4px" id="pass3" type="password" class="input" data-type="password">

                            <label style="margin-top: 10px" for="pass" class="label">Email Address</label>
                            <input style="margin-top: 4px" id="pass" type="text" class="input">

                            <input style="margin-top: 4px" type="button" class="button" value="Sign Up">

                        <div class="hr"></div>

                            <label for="tab-1"><a>Already Member?</a></label>

                    </form>
                </div>
            </div>

    </div>
</div>
</body>
</html>
